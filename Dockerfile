FROM mcr.microsoft.com/dotnet/aspnet:6.0

RUN apt update && apt install libvlc-dev vlc libx11-dev -y

WORKDIR /jukebox

COPY publish/Jukebox.Razor ./

EXPOSE 80

ENTRYPOINT ["dotnet", "Jukebox.Razor.dll"]
