using System;
using System.Threading.Tasks;
using Microsoft.Playwright.NUnit;
using NUnit.Framework;

namespace PlaywrightTests;

public class JukeboxTests : PageTest {
    private readonly string jukeboxUrl = "http://jukebox";
    [Test]
    public async Task CanAddTrack() {
        var track = BuildTrack();
        await Page.GotoAsync($"{jukeboxUrl}");

        await Page.ClickAsync("text=Upload Track");
        await Page.FillAsync("input#TrackName", track.Name);
        await Page.FillAsync("input#ArtistName", track.ArtistName);
        await Page.SetInputFilesAsync("input#Upload", track.FilePath);
        await Page.ClickAsync("text=Submit");

        await Page.ClickAsync("text=Track List");
        Assert.DoesNotThrowAsync(async () => await Page.Locator($"text={track.Name}").WaitForAsync());
    }
    [Test]
    public async Task CanQueueTrack() {
        var track = BuildTrack();
        await Page.GotoAsync($"{jukeboxUrl}");
        await Page.ClickAsync("text=Upload Track");
        await Page.FillAsync("input#TrackName", track.Name);
        await Page.FillAsync("input#ArtistName", track.ArtistName);
        await Page.SetInputFilesAsync("input#Upload", track.FilePath);
        await Page.ClickAsync("text=Submit");
        await Page.ClickAsync("text=Track List");

        var row = Page.Locator($"tr:has-text(\"{track.Name}\")");
        await row.Locator("button").ClickAsync();

        await Page.ClickAsync("text=Home");
        Assert.DoesNotThrowAsync(async () => await Page.Locator($"text={track.Name}").WaitForAsync());
    }
    [Test]
    public async Task CanPauseAndPlay() {
        var track = BuildTrack();
        await Page.GotoAsync($"{jukeboxUrl}");
        await Page.ClickAsync("text=Upload Track");
        await Page.FillAsync("input#TrackName", track.Name);
        await Page.FillAsync("input#ArtistName", track.ArtistName);
        await Page.SetInputFilesAsync("input#Upload", track.FilePath);
        await Page.ClickAsync("text=Submit");
        await Page.ClickAsync("text=Track List");
        var row = Page.Locator($"tr:has-text(\"{track.Name}\")");
        await row.Locator("button").ClickAsync();
        await Page.ClickAsync("text=Home");

        // can pause
        await Page.ClickAsync("#pause");
        await Page.ClickAsync("text=Home");

        Assert.DoesNotThrowAsync(async () => await Page.Locator("#play").WaitForAsync());

        // can play
        await Page.Locator("#play").ClickAsync();
        await Page.ClickAsync("text=Home");

        Assert.DoesNotThrowAsync(async () => await Page.Locator("#pause").WaitForAsync());
    }
    [Test]
    public async Task CanSkip() {
        await Page.GotoAsync($"{jukeboxUrl}");
        for(var i = 0; i < 2; i++) {
            var track = BuildTrack();
            await Page.ClickAsync("text=Upload Track");
            await Page.FillAsync("input#TrackName", track.Name);
            await Page.FillAsync("input#ArtistName", track.ArtistName);
            await Page.SetInputFilesAsync("input#Upload", track.FilePath);
            await Page.ClickAsync("text=Submit");
            await Page.ClickAsync("text=Track List");
            var row = Page.Locator($"tr:has-text(\"{track.Name}\")");
            await row.Locator("button").ClickAsync();
        }

        var text1 = await Page.Locator("#currentlyPlaying").TextContentAsync();
        await Page.ClickAsync("#skip");
        var text2 = await Page.Locator("#currentlyPlaying").TextContentAsync();

        Assert.That(text2, Is.Not.EqualTo(text1));
    }
    [Test]
    public async Task CanDeleteTrack() {
        var track = BuildTrack();
        await Page.GotoAsync($"{jukeboxUrl}");

        await Page.ClickAsync("text=Upload Track");
        await Page.FillAsync("input#TrackName", track.Name);
        await Page.FillAsync("input#ArtistName", track.ArtistName);
        await Page.SetInputFilesAsync("input#Upload", track.FilePath);
        await Page.ClickAsync("text=Submit");
        await Page.ClickAsync("text=Track List");

        var row = Page.Locator($"tr:has-text(\"{track.Name}\")");
        await row.Locator("text=Edit").ClickAsync();
        await Page.ClickAsync("text=Delete");

        await Page.ClickAsync("text=Track List");
        var el = await Page.Locator($"text={track.Name}").CountAsync();
        Assert.Zero(el);
    }

    record Track(string Name, string ArtistName, string FilePath);
    private Track BuildTrack() {
        var trackName = $"Track {Guid.NewGuid().ToString()[^5..]}";
        var artistName = $"Artist {Guid.NewGuid().ToString()[^5..]}";
        return new Track(trackName, artistName, "Track.ogg");
    }
}