using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using OneOf;
using OneOf.Types;

namespace Jukebox.Core;

public record Track(Guid Id, string Name, string ArtistName, byte[] TrackData);
public class Jukebox : IDisposable {
    private readonly object jukeboxLock = new();
    private readonly JukeboxQueue queue = new();
    private readonly ISpeaker speaker;
    private readonly Lazy<IDisposable> trackEnded;
    public Jukebox(ISpeaker speaker) {
        this.speaker = speaker ?? throw new ArgumentNullException(nameof(speaker));
        this.trackEnded = new Lazy<IDisposable>(() => 
            this.speaker.TrackEnded
            .Do(_ => Skip())
            .Subscribe());
    }
    public void Init() {
        _ = this.trackEnded.Value;
    }
    private OneOf<Track, None> currentlyPlaying = new None();
    public OneOf<Track, None> CurrentlyPlaying {
        get => this.currentlyPlaying;
    }
    public void Skip() {
        lock (this.jukeboxLock) {
            var next = this.queue.Dequeue();
            next.Switch(
                track => {
                    this.currentlyPlaying = track;
                    this.speaker.Play(track);
                },
                _ => {
                    this.currentlyPlaying = new None();
                }
            );
        }
    }
    public void Play() => this.speaker.Play();
    public void Pause() => this.speaker.Pause();
    public void Enqueue(Track track) {
        bool hasCurrentTrack = false;
        lock (this.jukeboxLock) {
            hasCurrentTrack = CurrentlyPlaying.IsT0;
            this.queue.Enqueue(track);
        }
        if (!hasCurrentTrack) { Skip(); }
    }
    public IReadOnlyList<Track> CurrentQueue() => this.queue.Tracks();
    public bool IsPlaying { get => this.speaker.IsPlaying; }
    public void Dispose() {
        this.trackEnded.Value.Dispose();
    }
}
public interface ISpeaker {
    public void Play(Track track);
    public void Play();
    public void Pause();
    public bool IsPlaying { get; }
    public IObservable<Unit> TrackEnded { get; }
}
public class JukeboxQueue {
    private readonly object queueLock = new();
    private readonly Queue<Track> queue = new();
    public void Enqueue(Track track) {
        lock (this.queueLock) {
            this.queue.Enqueue(track);
        }
    }
    public OneOf<Track,None> Dequeue() {
        lock (this.queueLock) {
            return this.queue.TryDequeue(out var track) ? track : new None();
        }
    }
    public IReadOnlyList<Track> Tracks() {
        lock (this.queueLock) {
            return this.queue.ToList();
        }
    }
}