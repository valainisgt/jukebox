using System;
using Microsoft.EntityFrameworkCore;

namespace Jukebox.Database;

public class Track {
    public Guid TrackId { get; set; }
    public string TrackName { get; set; }
    public string ArtistName { get; set; }
    public TrackFile File { get; set; }
}

public class TrackFile {
    public Guid TrackFileId { get; set; }
    public byte[] FileData { get; set; }
    public Guid TrackId { get; set; }
    public Track Track { get; set; }
}

public class JukeboxDbContext : DbContext {
    public JukeboxDbContext( DbContextOptions<JukeboxDbContext> options) : base (options) { }
    public DbSet<Track> Tracks { get; set; }
}