using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Jukebox.Database;

namespace Jukebox.Razor.Pages;
public class EditModel : PageModel {
    private readonly JukeboxDbContext db;
    public EditModel(JukeboxDbContext db) {
        this.db = db ?? throw new ArgumentNullException(nameof(db));
    }
    [BindProperty]
    public string TrackName { get; set; } = "";

    [BindProperty]
    public string ArtistName { get; set; } = "";

    [BindProperty(SupportsGet = true)]
    public Guid TrackId { get; set; }
    public async Task<IActionResult> OnGet() {
        var track = await this.db.Tracks.SingleOrDefaultAsync(x => x.TrackId == TrackId);
        if (track == null) {
            return NotFound();
        }

        TrackName = track.TrackName;
        ArtistName = track.ArtistName;

        return Page();
    }
    public async Task<IActionResult> OnPost() {
        var track = await this.db.Tracks.SingleOrDefaultAsync(x => x.TrackId == TrackId);
        if (track == null) {
            return NotFound();
        }

        this.db.Tracks.Remove(track);
        await this.db.SaveChangesAsync();

        return RedirectToPage("Tracks");
    }
}
