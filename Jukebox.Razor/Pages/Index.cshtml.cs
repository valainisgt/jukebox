﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Jukebox.Razor.Pages;

public class IndexModel : PageModel
{
    private readonly Jukebox.Core.Jukebox jukebox;
    public IndexModel(Jukebox.Core.Jukebox jukebox) {
        this.jukebox = jukebox ?? throw new ArgumentNullException(nameof(jukebox));
    }

    public void OnGet() {
        CurrentTrack = this.jukebox.CurrentlyPlaying.Match<Jukebox.Core.Track?>(x => x , _ => null);
        CurrentQueue = this.jukebox.CurrentQueue();
        IsPlaying = this.jukebox.IsPlaying;
    }
    public bool IsPlaying { get; private set; }
    public Jukebox.Core.Track? CurrentTrack { get; set; }
    public IReadOnlyList<Jukebox.Core.Track> CurrentQueue { get; set; } = new List<Jukebox.Core.Track>();

    public IActionResult OnPostPlay() {
        this.jukebox.Play();
        return RedirectToPage("Index");
    }
    public IActionResult OnPostPause() {
        this.jukebox.Pause();
        return RedirectToPage("Index");
    }
    public async Task<IActionResult> OnPostSkip() {
        this.jukebox.Skip();
        await Task.Delay(250); //artificial delay as libVLC doesn't say currently playing immediately
        return RedirectToPage("Index");
    }
}
