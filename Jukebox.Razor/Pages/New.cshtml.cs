using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Jukebox.Database;

namespace Jukebox.Razor.Pages;
public class NewModel : PageModel {
    private readonly JukeboxDbContext db;
    public NewModel(JukeboxDbContext db) {
        this.db = db ?? throw new ArgumentNullException(nameof(db));
    }
    [BindProperty, Required]
    public string TrackName { get; set; } = "";

    [BindProperty, Required]
    public string ArtistName { get; set; } = "";
    
    [BindProperty, Required]
    public IFormFile? Upload { get; set; }

    public void OnGet() { }
    public async Task<IActionResult> OnPost() {
        if (!ModelState.IsValid) {
            return Page();
        }
        if (Upload is null) { throw new ArgumentNullException(nameof(Upload)); }

        using var stream = new System.IO.MemoryStream();
        await Upload.CopyToAsync(stream);

        this.db.Tracks.Add(new Jukebox.Database.Track() {
            TrackName = TrackName,
            ArtistName = ArtistName,
            File = new TrackFile() {
                FileData = stream.ToArray()
            }
        });

        await this.db.SaveChangesAsync();

        return RedirectToPage("Index");
    }
}
