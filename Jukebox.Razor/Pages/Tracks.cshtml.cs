using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Jukebox.Database;

namespace Jukebox.Razor.Pages
{
    public class TracksModel : PageModel
    {
        private readonly JukeboxDbContext db;
        private readonly Jukebox.Core.Jukebox jukebox;
        public TracksModel(JukeboxDbContext db, Jukebox.Core.Jukebox jukebox) {
            this.db = db ?? throw new ArgumentNullException(nameof(db));
            this.jukebox = jukebox ?? throw new ArgumentNullException(nameof(jukebox));
        }
        public IList<Track> Tracks { get; set; } = new List<Track>();
        public async Task OnGetAsync () {
            Tracks = await this.db.Tracks.ToListAsync();
        }
        public async Task<IActionResult> OnPostAsync(Guid id) {
            var track = await this.db.Tracks.Include(x => x.File).SingleAsync(x => x.TrackId == id);
            this.jukebox.Enqueue(new Jukebox.Core.Track(track.TrackId, track.TrackName, track.ArtistName, track.File.FileData));
            await Task.Delay(250); //artificial delay as libVLC doesn't say currently playing immediately
            return RedirectToPage("Index");
        }
    }
}
