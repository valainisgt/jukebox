using System.Reactive;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using LibVLCSharp.Shared;
using Microsoft.EntityFrameworkCore;
using Jukebox.Core;

var builder = WebApplication.CreateBuilder(args);

var defaultDataPath = Path.Combine(
    Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData),
    "valainisgt",
    "jukebox"
);
var dataPath =  builder.Configuration["jukebox:dataPath"] ?? defaultDataPath;
var databasePath = Path.Combine(dataPath, "jukebox.db");

builder.Services.AddRazorPages();

builder.Services.AddDbContext<Jukebox.Database.JukeboxDbContext>(options => {
    options.UseSqlite($"Data Source={databasePath}");
});
var libvlc = new LibVLC();
builder.Services.AddSingleton(libvlc);
builder.Services.AddSingleton<ISpeaker,LibVLCSpeaker>();
builder.Services.AddSingleton<Jukebox.Core.Jukebox>(sp => {
    var jukebox = new Jukebox.Core.Jukebox(
        sp.GetRequiredService<ISpeaker>()
    );
    jukebox.Init();
    return jukebox;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment()) {
    app.UseExceptionHandler("/Error");
}

app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

if (!File.Exists(databasePath)) {
    Directory.CreateDirectory(dataPath);
}

using (var scope = app.Services.CreateScope())
using (var db = scope.ServiceProvider.GetRequiredService<Jukebox.Database.JukeboxDbContext>()) {
    await db.Database.EnsureCreatedAsync();
}

app.Run();

class LibVLCSpeaker : ISpeaker, IDisposable {
    private readonly LibVLC libVLC;
    private readonly MediaPlayer mediaPlayer;
    private readonly object mediaLock = new();
    public LibVLCSpeaker(LibVLC libVLC) : this(libVLC, System.Reactive.Concurrency.ThreadPoolScheduler.Instance) { }
    public LibVLCSpeaker(LibVLC libVLC, IScheduler scheduler) {
        this.libVLC = libVLC ?? throw new ArgumentNullException(nameof(libVLC));
        this.mediaPlayer = new MediaPlayer(this.libVLC);
        this.TrackEnded = Observable.FromEventPattern<EventArgs>(
            h => this.mediaPlayer.EndReached += h,
            h => this.mediaPlayer.EndReached -= h
        ).Select(x => Unit.Default).ObserveOn(scheduler);
    }
    public void Play(Track track) {
        lock (this.mediaLock) {
            using (var media = new Media(this.libVLC, new StreamMediaInput(new MemoryStream(track.TrackData, false)), ":no-video")) {
                this.mediaPlayer.Media = media;
            }
            this.mediaPlayer.Play();
        }
    }
    public void Play() {
        lock (this.mediaLock) {
            this.mediaPlayer.Play();
        }
    }
    public void Pause() {
        lock (this.mediaLock) {
            this.mediaPlayer.Pause();
        }
    }
    public bool IsPlaying { get => this.mediaPlayer.IsPlaying; }
    public IObservable<Unit> TrackEnded { get; }
    public void Dispose() {
        this.mediaPlayer.Dispose();
    }
}
