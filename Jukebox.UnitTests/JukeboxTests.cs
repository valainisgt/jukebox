using System;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using OneOf.Types;
using Xunit;
using Jukebox.Core;

namespace Jukebox.UnitTests;

public class JukeBoxTests {
    [Fact]
    public void AddingSongToEmptyQueueStartsTheSong() {
        var sut = new Jukebox.Core.Jukebox(new FakeSpeaker());
        sut.Init();

        Assert.Equal(new None(), sut.CurrentlyPlaying.AsT1);

        var track = new Track(Guid.NewGuid(), "Song", "Artist", Array.Empty<byte>());
        sut.Enqueue(track);

        Assert.Equal(track, sut.CurrentlyPlaying.AsT0);
    }
}

class FakeSpeaker : ISpeaker {
    private readonly Subject<Unit> subject;
    public FakeSpeaker() : this(TimeSpan.FromSeconds(2)) { }
    public FakeSpeaker(TimeSpan t) {
        this.subject = new Subject<Unit>();
        this.TrackEnded = this.subject.AsObservable().Delay(t);
    }
    public void Play(Track track) {
        this.subject.OnNext(Unit.Default);
    }
    public void Play() { }
    public void Pause() { }
    public bool IsPlaying { get => false; }
    public IObservable<Unit> TrackEnded { get; }
}