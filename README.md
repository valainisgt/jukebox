# Jukebox
Turn your old computer or SBC into a jukebox!

![Jukebox screenshot](https://gitlab.com/valainisgt/jukebox/-/raw/master/assets/screenshot.png)

## Overview
Jukebox is a self hosted jukebox application.
Use a web interface to upload music, queue up tracks, and blast some tunes!

## Installation
In order to get started you'll need the [.NET 6 runtime](https://dotnet.microsoft.com/en-us/download/dotnet/6.0) installed.

Download and extract the latest release from [the releases page](https://gitlab.com/valainisgt/jukebox/-/releases).

Run Jukebox with the following command: `dotnet Jukebox.Razor.dll`

Once started you can visit the jukebox at [http://localhost:5000](http://localhost:5000).

If running on linux you may need some other packages. Check out [this link for more information](https://code.videolan.org/videolan/LibVLCSharp/-/blob/3.x/docs/linux-setup.md).

We also provide a Docker image. The image is hosted in this project's [container registry](https://gitlab.com/valainisgt/jukebox/container_registry).
For help playing sound from a Docker container [check out this article](https://blog.jessfraz.com/post/docker-containers-on-the-desktop/#6-spotify).

## Configuration
Jukebox is not bundled with a proper web server and should be setup behind a reverse-proxy for any use case other than localhost usage.
Jukebox by default listens on URLs `http://localhost:5000` and `https://localhost:5001`.
If you wish to change which URLs are listened on you can set an environment variable `ASPNETCORE_URLS` to your desired addresses.
Multiple addresses must be separated by a semicolon, eg `ASPNETCORE_URLS="http://localhost:8080;https://localhost:8081"`.

## Support
If you have a feature you'd like to see implemented or a bug you'd like to report,
please [create an issue](https://gitlab.com/valainisgt/jukebox/-/issues/new).

## Contributing
If you are a developer and would like to contribute to the project, send me a merge request to start a conversation.

## License
Jukebox is free software licensed under [The GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.txt).